import { Component, AfterViewInit, ViewChild, OnInit } from '@angular/core';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  @ViewChild(CdkVirtualScrollViewport, { static: false }) viewport: CdkVirtualScrollViewport;

  public data: any[] = [];
  public pageNumber = 0;
  public allRecordLoaded = false;
  public isLoading = false;

  public style = {
    height: 250 + 'px'
  };

  constructor(
    private httpClient: HttpClient,
  ) { }

  ngAfterViewInit() {
    this.viewport.elementRef.nativeElement.onscroll = (e) => { this.onScroll(e); };
  }
  onScroll(e) {
    const scrollOffset = this.viewport.measureScrollOffset('bottom');
    if (scrollOffset === 0) {
      this.getData();
    }
  }

  ngOnInit() {
    this.getData();
  }
  getData() {
    if (this.allRecordLoaded && this.isLoading) {
      return false;
    }
    this.isLoading = true;
    this.pageNumber++;
    this.httpClient.get('https://api.github.com/search/repositories?q=angular&per_page=10&page=' + this.pageNumber)
      .subscribe((results: any) => {
        this.data = this.data.concat(results.items);
        if (results.items.length === 0) {
          this.allRecordLoaded = true;
        }
        this.isLoading = false;
      });
  }
}
